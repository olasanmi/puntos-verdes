import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  year: number;
  items: MenuItem[];

  constructor() {
    this.year = new Date().getFullYear();
   }

  ngOnInit(): void {
    this.items = [
      {
        items: [
          {label: 'Home', url: 'https://www.fundacionpuntosverdes.com/'},
          {label: 'Conócenos', url: 'https://www.fundacionpuntosverdes.com/conocenos/'},
          {
            label: 'Posconsumo',
            url: 'https://www.fundacionpuntosverdes.com/posconsumo/',
            target: '__blank'
          },
          {label: 'Puntos de recolección', url: 'https://www.fundacionpuntosverdes.com/puntos-de-recoleccion/'},
          { label: 'Causas sociales y ambientales', url: 'https://www.fundacionpuntosverdes.com/causas-sociales-y-ambientales/', target: '__blank' },
          {label: 'Blog', url: 'https://www.fundacionpuntosverdes.com/blog/'}
        ]
      },
      {
        items: [
          {label: 'Voluntariado', url: 'https://www.fundacionpuntosverdes.com/voluntariado/'},
          {label: 'Inicia sesión', url: 'https://www.fundacionpuntosverdes.com/app/'}
        ]
      },
    ];
  }

}
