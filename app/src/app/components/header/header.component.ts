import { Component, OnDestroy, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  items: MenuItem[];
  isLogged = false;

  private subscription: Subscription = new Subscription();
  private destroy$ = new Subject<any>();

  constructor(public authSvc: AuthService, private router: Router) {}



  doAlert = () => {
  }

  ngOnInit(): void {
    this.items = [
      {
          label: 'HOME',
          url: 'https://www.fundacionpuntosverdes.com/',
          target: '__blank'
      },
      {
          label: 'CONÓCENOS',
          url: 'https://www.fundacionpuntosverdes.com/conocenos/',
          target: '__blank'
      },
      {
        label: 'POSCONSUMO',
        url: 'https://www.fundacionpuntosverdes.com/posconsumo/',
        target: '__blank'
      },
      {
        label: 'PUNTOS DE RECOLECCIÓN',
        url: 'https://www.fundacionpuntosverdes.com/puntos-de-recoleccion/',
        target: '__blank'
      },
      {
        label: 'CAUSAS SOCIALES Y AMBIENTALES',
        url: 'https://www.fundacionpuntosverdes.com/causas-sociales-y-ambientales/',
        target: '__blank'
      },
      {
        label: 'BLOG',
        url: 'https://www.fundacionpuntosverdes.com/blog/',
        target: '__blank'
      },
      {
        label: 'DOCUMENTACIÓN',
        url: 'https://www.fundacionpuntosverdes.com/voluntariado/,',
        target: '__blank'
      }
    ];

    this.subscription.add(
      this.authSvc.isLogged.subscribe( (res) => (this.isLogged = res))
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onLogout(): void {
    this.authSvc.logout();
    this.router.navigate(['/login']);
  }

}
